\0\0P: power on...
P: init stack done
P: check reset
N: wait for scp ready
P: chip_id : KPA2912016130000
N: 5: c0000000; 6: c0000000; 7: c0000003; 4: 80000002
P: Booting Trusted Firmware
P: BL1: v2.3(release):E2000-v1.03
P: BL1: Built : 11:14:15, Dec  1 2022
P: BL1: RAM 0x30c01000 - 0x30c14000
P: Generic delay timer configured with mult=1 and div=50
P: Entry point address = 0x38180000
P: SPSR = 0x3c9
P: Argument #0 = 0x0
P: Argument #1 = 0x0
P: Argument #2 = 0x0
P: Argument #3 = 0x0
P: Argument #4 = 0x0
P: Argument #5 = 0x0
P: Argument #6 = 0x0
P: Argument #7 = 0x0
flash tpye = default flash
get flash speed = 3
flash set done
N: pre svc call fun = 0xc2000f02 -- pm-1 = 0, pm-2 = 30c19930, pm-3 = 0
N: CPU info : 
N:  magic = 0x54460020
N:  version = 0x2
N:  size = 0x100
N:  clust0_pll = 0x7d0
N:  clust1_pll = 0x7d0
N:  clust2_pll = 0x5dc
N:  dmu_pll  = 0x258
N: pll config
Parametr :pll 0  frq = 2000
Parametr :pll 1  frq = 2000
Parametr :pll 2  frq = 1500
Parametr :pll 3  frq = 1200
Parametr :pll 4  frq = 600
N: reg_pll_c0_real = 2000 MHZ 
N: reg_pll_c1_real = 2000 MHZ 
N: reg_pll_c2_real = 1500 MHZ 
N: reg_pll_dmu_0_real = 600 MHZ 
N: scmi pll config 
get the ch->info->scmi_mbx_mem is 0x32a10400
N: get the return= 0x0
parameter pad_base = 0x380fa000
value = 0x140, addr = 0x32b30000
value = 0x240, addr = 0x32b30020
value = 0x40, addr = 0x32b30024
value = 0x40, addr = 0x32b30028
value = 0x40, addr = 0x32b3002c
value = 0x240, addr = 0x32b30030
value = 0x240, addr = 0x32b30034
value = 0x40, addr = 0x32b30038
value = 0x40, addr = 0x32b3003c
value = 0x40, addr = 0x32b30040
value = 0x40, addr = 0x32b30044
value = 0x40, addr = 0x32b30048
value = 0x140, addr = 0x32b3004c
value = 0x140, addr = 0x32b30050
value = 0x140, addr = 0x32b30054
value = 0x140, addr = 0x32b30058
value = 0x140, addr = 0x32b3005c
value = 0x140, addr = 0x32b30060
value = 0x141, addr = 0x32b30064
value = 0x141, addr = 0x32b30068
value = 0x145, addr = 0x32b3006c
value = 0x240, addr = 0x32b30070
value = 0x242, addr = 0x32b30074
value = 0x242, addr = 0x32b30078
value = 0x240, addr = 0x32b3007c
value = 0x240, addr = 0x32b30080
value = 0x240, addr = 0x32b30084
value = 0x240, addr = 0x32b30088
value = 0x40, addr = 0x32b3008c
value = 0x45, addr = 0x32b30090
value = 0x40, addr = 0x32b30094
value = 0x40, addr = 0x32b30098
value = 0x40, addr = 0x32b3009c
value = 0x40, addr = 0x32b300a0
value = 0x240, addr = 0x32b300a4
value = 0x44, addr = 0x32b300a8
value = 0x44, addr = 0x32b300ac
value = 0x240, addr = 0x32b300b0
value = 0x40, addr = 0x32b300b4
value = 0x45, addr = 0x32b300b8
value = 0x41, addr = 0x32b300bc
value = 0x140, addr = 0x32b300c0
value = 0x140, addr = 0x32b300c4
value = 0x245, addr = 0x32b300c8
value = 0x245, addr = 0x32b300cc
value = 0x245, addr = 0x32b300d0
value = 0x245, addr = 0x32b300d4
value = 0x240, addr = 0x32b300d8
value = 0x40, addr = 0x32b300dc
value = 0x240, addr = 0x32b300e0
value = 0x240, addr = 0x32b300e4
value = 0x40, addr = 0x32b300e8
value = 0x40, addr = 0x32b300ec
value = 0x240, addr = 0x32b300f0
value = 0x240, addr = 0x32b300f4
value = 0x240, addr = 0x32b300f8
value = 0x46, addr = 0x32b300fc
value = 0x143, addr = 0x32b30100
value = 0x243, addr = 0x32b30104
value = 0x246, addr = 0x32b30108
value = 0x240, addr = 0x32b3010c
value = 0x240, addr = 0x32b30110
value = 0x240, addr = 0x32b30114
value = 0x240, addr = 0x32b30118
value = 0x240, addr = 0x32b3011c
value = 0x40, addr = 0x32b30120
value = 0x246, addr = 0x32b30124
value = 0x46, addr = 0x32b30128
value = 0x46, addr = 0x32b3012c
value = 0x46, addr = 0x32b30130
value = 0x42, addr = 0x32b30134
value = 0x242, addr = 0x32b30138
value = 0x242, addr = 0x32b3013c
value = 0x242, addr = 0x32b30140
value = 0x242, addr = 0x32b30144
value = 0x242, addr = 0x32b30148
value = 0x242, addr = 0x32b3014c
value = 0x246, addr = 0x32b30150
value = 0x244, addr = 0x32b30154
value = 0x244, addr = 0x32b30158
value = 0x244, addr = 0x32b3015c
value = 0x244, addr = 0x32b30160
value = 0x244, addr = 0x32b30164
value = 0x244, addr = 0x32b30168
value = 0x243, addr = 0x32b3016c
value = 0x243, addr = 0x32b30170
value = 0x243, addr = 0x32b30174
value = 0x43, addr = 0x32b30178
value = 0x243, addr = 0x32b3017c
value = 0x143, addr = 0x32b30180
value = 0x143, addr = 0x32b30184
value = 0x143, addr = 0x32b30188
value = 0x41, addr = 0x32b3018c
value = 0x41, addr = 0x32b30190
value = 0x41, addr = 0x32b30194
value = 0x41, addr = 0x32b30198
value = 0x241, addr = 0x32b3019c
value = 0x41, addr = 0x32b301a0
value = 0x41, addr = 0x32b301a4
value = 0x41, addr = 0x32b301a8
value = 0x41, addr = 0x32b301ac
value = 0x41, addr = 0x32b301b0
value = 0x41, addr = 0x32b301b4
value = 0x41, addr = 0x32b301b8
value = 0x41, addr = 0x32b301bc
value = 0x41, addr = 0x32b301c0
value = 0x45, addr = 0x32b301c4
value = 0x45, addr = 0x32b301c8
value = 0x44, addr = 0x32b301cc
value = 0x44, addr = 0x32b301d0
value = 0x44, addr = 0x32b301d4
value = 0x44, addr = 0x32b301d8
value = 0x44, addr = 0x32b301dc
value = 0x44, addr = 0x32b301e0
value = 0x44, addr = 0x32b301e4
value = 0x44, addr = 0x32b301e8
value = 0x43, addr = 0x32b301ec
value = 0x43, addr = 0x32b301f0
value = 0x46, addr = 0x32b301f4
value = 0x46, addr = 0x32b301f8
value = 0x46, addr = 0x32b301fc
value = 0x42, addr = 0x32b30200
value = 0x42, addr = 0x32b30204
value = 0x42, addr = 0x32b30208
value = 0x42, addr = 0x32b3020c
value = 0x41, addr = 0x32b30210
value = 0x41, addr = 0x32b30214
value = 0x41, addr = 0x32b30218
value = 0x41, addr = 0x32b3021c
value = 0x41, addr = 0x32b30220
value = 0x40, addr = 0x32b30224
value = 0x40, addr = 0x32b30228
value = 0x40, addr = 0x32b3022c
value = 0x40, addr = 0x32b30230
value = 0x40, addr = 0x32b30234
value = 0x42, addr = 0x32b30238
value = 0x40, addr = 0x32b3023c
value = 0x46, addr = 0x32b30240
value = 0x46, addr = 0x32b30244
value = 0x46, addr = 0x32b30248
value = 0x46, addr = 0x32b3024c
N: pre svc call fun = 0xc2000f03 -- pm-1 = 0, pm-2 = 30c19930, pm-3 = 0
N:  pcie fun = 0x0 -- pm addr = 30c19930 
N: PCIE info : 
N:  magic = 0x54460021
N:  version = 0x4
N:  size = 0x100
N:  independent_tree = 0x0
N:  base_cfg = 0x10005
N:  size = 0x100
N:  PEU 0 - C0 base_config = 0x10203
N:  PEU 0 - C1 base_config = 0x10003
N:  PEU 0 - C0 eq = 0x7
N:  PEU 0 - C1 eq = 0x7
N:  PEU 1 - C0 base_config = 0x10003
N:  PEU 1 - C1 base_config = 0x10203
N:  PEU 1 - C2 base_config = 0x10203
N:  PEU 1 - C3 base_config = 0x10203
N:  PEU 1 - C0 eq = 0x7
N:  PEU 1 - C1 eq = 0x7
N:  PEU 1 - C2 eq = 0x7
N:  PEU 1 - C3 eq = 0x7
I: PEU 0 pre_init 
I: Parametr : peu 0  mode 0x3
I: set peu0 bif is 0x0
I: Parametr : peu 0  speed 0xf
I: Parametr : peu 0 ctr 0  mode 0x1
I: Parametr : peu 0 ctr 1  mode 0x1
I: set psu phy reg
I: Parametr : peu 0 init stat 0x1
N: PEU 0 phy init 
I: Parametr : peu 0 split mode 0x2
I: peu0 c1 phy init.
I: read peu0 clk_status.
I: peu clk_status1 0xd, target clk_status 0xf
I: peu clk_status2 0xd
I: Parametr : peu 1 init stat 0x1
I: PEU 1 pre_init 
I: Parametr : peu 1  mode 0xf
I: Parametr : peu 1 split mode 0x0
I: set peu1 bif is 0x0
I: Parametr : peu 1  speed 0xff
I: Parametr : peu 1 ctr 0  mode 0x1
I: Parametr : peu 1 ctr 1  mode 0x1
I: Parametr : peu 1 ctr 2  mode 0x1
I: Parametr : peu 1 ctr 3  mode 0x1
N: PEU 1 phy init 
I: read peu1 clk_status.
I: peu clk_status1 0xff, target clk_status 0x57
I: peu clk_status2 0xff
I: Parametr : independent_tree  --- 0x0
Parametr : peu 0  port stat 0x1
I: Parametr : peu 0 split mode 0x2
I: set peu msien 0x0
I: ocn msien 0x10000
I: Parametr : peu 0 ctr 0  mode 0x1
I: Parametr : peu 0 ctr 1  mode 0x1
I: Parametr : peu 1 init stat 0x1
I: Parametr : independent_tree  --- 0x0
Parametr : peu 1  port stat 0xe
I: set peu msien 0x0
I: ocn msien 0x10000
I: Parametr : peu 1 ctr 0  mode 0x1
vhub device set!
N: pre svc call fun = 0xc2000015 -- pm-1 = 3f, pm-2 = 588, pm-3 = 3000300
usb 0x31b00000 init.
sgmii0 init.
set mode: 0x3, speed: 0x3 phy_base: 0x32100000, ctrl_base: 0x3200c000.
1
0x32100000 .
7
gsd sata phy init.
SATA 1 phy init seq start.
gsd_phy2 dp0 nothing.
gsd_phy3 dp1 nothing.


U-Boot 2022.01-v1.40 (Jan 12 2023 - 09:42:11 +0800)

DRAM:  Phytium ddr init 
  CH 0 : 
dimm info from spd, read spd...
Read Spd Begin...
Check Dram Type ...
Parse SPD Data ...
  tAAmim    = 13750ps
  tRCDmin   = 13750ps
  tRPmin    = 13750ps
  tRASmin   = 32000ps
  tRCmin    = 45750ps
  tFAWmin   = 21000ps
  tRRD_Smin = 3000ps
  tRRD_Lmin = 4900ps
  tCCD_Lmin = 5000ps
  tWRmin    = 15000ps
  tWTR_Smin = 2500ps
  tWTR_Lmin = 7500ps
  tRFC1min  = 350000ps
  tRFC2min  = 260000ps
  tRFC4min  = 160000ps
	Dimm_Capacity = 4GB
	DDR4	SODIMM/4 Bank Groups/4 Banks/Column 10/Row 15/X8/1 Rank/NO ECC/Standard
	Modual:Unknown=0x8968/Dram:Hynix/Serial:0x387
N: pre svc call fun = 0xc2000f04 -- pm-1 = 0, pm-2 = 30c19930, pm-3 = 0
N:  ddr fun = 0x0 -- pm = 0x30c19930, pm2 = 0x0
N: parameter mcu: v0.2

Mcu Start Work ...
N: scmi get clock value. 
get clock value 0x23c34600 600000000
CTL Freq = 600Mhz
Lmu Freq = 1200Mhz
get s3 = 0
	ch = 0
the dimm info is from uboot...
F0RC03 = 0x0
F0RC04 = 0x0
F0RC05 = 0x0
F0RC03 = 0x0
F0RC04 = 0x0
F0RC05 = 0x0
F0RC03 = 0x0
F0RC04 = 0x0
F0RC05 = 0x0
parameter set ecc disable!
	Dimm_Capacity = 4GB
MEM encrypt configuration begin...
MEM encrypt bypass end...
TZC configuration begin...
TZC bypass end...
use_0x10 == 0x30004
use_0x14 == 0x30100
ctl_cfg_begin......
pi_cfg_begin......
phy_cfg_begin......
fast mode
caslat = 17
wrlat = 16
tinit = 960000
r2r_diffcs_dly = 4
r2w_diffcs_dly = 5
w2r_diffcs_dly = 3
w2w_diffcs_dly = 7
r2w_samecs_dly = 4
w2r_samecs_dly = 0
r2r_samecs_dly = 0
w2w_samecs_dly = 0
	adapter_alg -- 0-0-0-0-0-0-0
rtt_wr = dis
rtt_park = 80ohm
ron = 34ohm
val_cpudrv = 34
rtt_nom = 48ohm
val_cpuodt = 60
vref_dev = 10
vrefsel = 0x3c
dq_oe_timing = 0x42
rank_num_decode = 1
set phy_indep_init_mode
set pi_dram_init_en
set_pi_start & ctl_start......
wait init complete......
init complete done......
wait complete done......
rddqs_lat = 0x3
tdfi_phy_rdlat = 0x20
begin software ntp training...
rank_num: 0
phy_write_path_lat_add =-1-1-1-1-1-1-1-1-1


slice_num 0

slice_num 1

slice_num 2

slice_num 3

slice_num 4

slice_num 5

slice_num 6

slice_num 7
phy_write_path_lat_add = 0 0 0 0 0 0 0 0-1


slice_num 0
pi_bist data bist pass!
start_addr = 0x80000000
end_addr = 0x80000100

slice_num 1
pi_bist data bist pass!
start_addr = 0x80000000
end_addr = 0x80000100

slice_num 2
pi_bist data bist pass!
start_addr = 0x80000000
end_addr = 0x80000100

slice_num 3
pi_bist data bist pass!
start_addr = 0x80000000
end_addr = 0x80000100

slice_num 4

slice_num 5

slice_num 6

slice_num 7
phy_write_path_lat_add = 0 0 0 0 1 1 1 1-1


slice_num 0
pi_bist data bist pass!
start_addr = 0x80000000
end_addr = 0x80000100

slice_num 1
pi_bist data bist pass!
start_addr = 0x80000000
end_addr = 0x80000100

slice_num 2
pi_bist data bist pass!
start_addr = 0x80000000
end_addr = 0x80000100

slice_num 3
pi_bist data bist pass!
start_addr = 0x80000000
end_addr = 0x80000100

slice_num 4
pi_bist data bist pass!
start_addr = 0x80000000
end_addr = 0x80000100

slice_num 5
pi_bist data bist pass!
start_addr = 0x80000000
end_addr = 0x80000100

slice_num 6
pi_bist data bist pass!
start_addr = 0x80000000
end_addr = 0x80000100

slice_num 7
pi_bist data bist pass!
start_addr = 0x80000000
end_addr = 0x80000100
rank 0 wdqlvl!
r2r_diffcs_dly = 4
r2w_diffcs_dly = 6
w2r_diffcs_dly = 4
w2w_diffcs_dly = 6
r2w_samecs_dly = 4

rank 0
		training success


ctl-bist: addr_space = 0x14
bist test start...
data bist pass!
bist test end...

ctl-bist: addr_space = 0x14
bist test start...
addr bist pass!
bist test end...
BIST OK, access memory!

skip_scrub_flag = 0, scrub init
start_addr = 0x0, addr_space = 32
zero memory start...
zero memory end
DBI mode enable
N: pre svc call fun = 0xc2000f05 -- pm-1 = 0, pm-2 = 30c19930, pm-3 = 0
N: CPU info : 
N:  magic = 0x54460013
N:  version = 0x1
N:  size = 0x100
N:  core_bit_map = 0x311
I: BL1: Loading BL2
V: Using Memmap
V: FIP header looks OK.
V: Using FIP
I: Loading image id=1 at address 0xffe00000
I: Image id=1 loaded: 0xffe00000 - 0xffe0c61d
V: BL1: BL2 memory layout address = 0x30c00000
N: BL1: Booting BL2
P: Entry point address = 0xffe00000
P: SPSR = 0x3c5
P: Argument #0 = 0x0
P: Argument #1 = 0x30c00000
P: Argument #2 = 0x0
P: Argument #3 = 0x0
P: Argument #4 = 0x0
P: Argument #5 = 0x0
P: Argument #6 = 0x0
P: Argument #7 = 0x0
N: BL2: v2.3(release):E2000-v1.03
N: BL2: Built : 11:14:15, Dec  1 2022
I: BL2: Doing platform setup
N: img_id: 4, image_base: 0x38100000, img_size: 0x10000
N: Copy BL32 image from: 0x38100000 to: 0xfc000000 size: 0x10000
I: BL2: Loading image id 3
V: Using Memmap
V: FIP header looks OK.
V: Using FIP
I: Loading image id=3 at address 0xffa00000
I: Image id=3 loaded: 0xffa00000 - 0xffa1234a
I: BL2: Skip loading image id 23
I: BL2: Skip loading image id 25
I: BL2: Skip loading image id 4
I: BL2: Skip loading image id 21
I: BL2: Skip loading image id 5
I: BL2: Skip loading image id 27
N: BL1: Booting BL31
P: Entry point address = 0xffa00000
P: SPSR = 0x3cd
P: Argument #0 = 0xffe0d760
P: Argument #1 = 0x0
P: Argument #2 = 0x0
P: Argument #3 = 0x0
P: Argument #4 = 0x0
P: Argument #5 = 0x0
P: Argument #6 = 0x0
P: Argument #7 = 0x0
P: entry bl31 form bl2.
N: bl_params - x0 = ffe0d760 
N: TLB: start setup TLB
mmap:
 VA:0x38040000  PA:0x38040000  size:0x100000  attr:0x1  granularity:0x40000000
 VA:0x0  PA:0x0  size:0x80000000  attr:0x8  granularity:0x40000000
 VA:0xfc000000  PA:0xfc000000  size:0x2000000  attr:0xa  granularity:0x40000000
 VA:0xffa00000  PA:0xffa00000  size:0x12000  attr:0x2  granularity:0x40000000
 VA:0xffa1f000  PA:0xffa1f000  size:0x1000  attr:0x8  granularity:0x40000000
 VA:0xffa00000  PA:0xffa00000  size:0x600000  attr:0xa  granularity:0x40000000
 VA:0x80000000  PA:0x80000000  size:0x80000000  attr:0x1a  granularity:0x40000000

V: Translation tables state:
V:   Xlat regime:     EL3
V:   Max allowed PA:  0xffffffff
V:   Max allowed VA:  0xffffffff
V:   Max mapped PA:   0xffffffff
V:   Max mapped VA:   0xffffffff
V:   Initial lookup level: 1
V:   Entries @initial lookup level: 4
V:   Used 4 sub-tables out of 4 (spare: 0)
N: start enable mmu
N: mmu setup done
N: TLB: start setup TLB
N: BL31: v2.3(release):E2000-v1.03
N: BL31: Built : 11:14:18, Dec  1 2022
I: GICv3 without legacy support detected.
I: ARM GICv3 driver initialized in EL3
P: Generic delay timer configured with mult=1 and div=50
N: PFDI: start  setup 
N: GMAC addr setup-mac force  N: mac = 553721160c
I: BL31: Initializing runtime services
I: BL31: Initializing BL32
W: BL31: BL32 initialization failed
I: BL31: Preparing for EL3 exit to normal world
I: Entry point address = 0x381c0000
I: SPSR = 0x3c9
V: Argument #0 = 0x200
V: Argument #1 = 0x0
V: Argument #2 = 0x0
V: Argument #3 = 0x0
V: Argument #4 = 0x0
V: Argument #5 = 0x0
V: Argument #6 = 0x0
V: Argument #7 = 0x0
N: cm temp context restore 
N: runtime setup 
N: 5: c0000000; 6: c0000000; 7: c0000003; 4: 80000002
N: wait for scp ready
N: 5: c0000000; 6: c0000000; 7: c0000003; 4: 80000002
N: enable scp->ap os chanel interrupt. 
N: [os chanel] = 1 
N: enable ap->scp os chanle interrupt. 
N: [os chanel] = 1 
N: scmi_core_reset_addr_set. 
PBF relocate done 
system_off_entry addr =0x3818a628
system_reset_entry addr =0x3818a640
suspend_entry addr =0x3818a670
suspend_end_entry addr =0x3818a68c
suspend_finish_entry addr =0x3818a658
N: Phytium System Service Call: 0xc2000012 
N: PFDI: get vectors : addr 30c19a08
N: PFDI: system_off_entry :  3818a628
N: PFDI: system_reset_entry :  3818a640
N: PFDI: suspend_start_entry :  3818a670
N: PFDI: suspend_end_entry :  3818a68c
N: PFDI: suspend_finish_entry :  3818a658
1.9 GiB
MMC:   clk = 1200000000Hz
clk = 1200000000Hz
PHYTIUM MCI: 0, PHYTIUM MCI: 1
Loading Environment from SPIFlash... *** Warning - bad CRC, using default environment

In:    uart@2800d000
Out:   uart@2800d000
Err:   uart@2800d000
Net:   eth0: ethernet@3200c000, eth1: ethernet@32010000
  pci_0:1.0         00:01.00  - 1db7:dc01     pcie@40000000    0x06 (Bridge device)
  pci_0:2.0         00:02.00  - 1db7:dc01     pcie@40000000    0x06 (Bridge device)
scanning bus for devices...
SATA link 0 timeout.
AHCI 0001.0301 32 slots 1 ports 6 Gbps 0x1 impl SATA mode
flags: 64bit ncq stag pm led clo only pmp pio slum part ccc apst 
SATA link 0 timeout.
AHCI 0001.0301 32 slots 1 ports 6 Gbps 0x1 impl SATA mode
flags: 64bit ncq stag pm led clo only pmp pio slum part ccc apst 
Hit any key to stop autoboot:  2  1  0 
Couldn't find partition scsi 0:2
Can't set block device
Couldn't find partition scsi 0:2
Can't set block device
Couldn't find partition scsi 0:2
Can't set block device
Wrong Image Format for bootm command
ERROR: can't get kernel image!
E2000#
E2000#
E2000#
E2000#
E2000#