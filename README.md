# 飞腾平台芯片测试固件(SFW)和开机启动log

#### 介绍
汇总整理飞腾平台各个芯片测试固件(SFW)和开机启动log，辅助飞腾伙伴对比调试使用。

#### 软件架构
飞腾平台固件SFW（System Firmware）包含UEFI和uboot两种，签署NDA后，均可从FAE处获取源码。

飞腾平台固件需要与飞腾各个芯片的PBF（Processor Base Firmware）打包工具配合使用，相关系统软件栈如下图所示：

![输入图片说明](PBF.png)

基于 Processor Base Firmware 的系统软件栈如上图所示。

其中，固件分为三层：飞腾芯片内置可信根（Phytium Boot ROM， PBR）、飞腾处理器基础固件（Processor Base Firmware， PBF）和系统固件（System Firmware， SFW）。

更多详细规范请参照飞腾官方《Processor Base Firmware接口规范》

#### 使用说明

 **1.  飞腾平台固件SFW（System Firmware）** 

 **1.1   飞腾平台固件说明** 

飞腾平台固件SFW（System Firmware）包含UEFI和uboot，本仓库均包含两个文件，一个BIN文件，一个TXT文件，都是飞腾PBF工具打包而生成的。

BIN文件是烧录使用的二进制文件，由PBF打包工具./my_scripts/image-fix.sh（不同芯片命令可能有差异，以飞腾提供的的说明文件为准）命令生成的fip-all.bin文件重命名而来；

TXT文件是描述对应BIN文件详细配置的说明文件，由PBF打包工具./my_scripts/image-fix.sh命令生成的./all/project.log文件重命名而来。


 **1.2   文件名命名规范** 

BIN文件文件名：ChipID-DDR-PBFvxx-UEFI/ubootvxxx-CPLD/EC/SE-debug/release.bin  

TXT文件文件名：ChipID-DDR-PBFvxx-UEFI/ubootvxxx-CPLD/EC/SE-debug/release.txt 

说明：

ChipID--飞腾芯片型号如e2000q、e2000d、e2000s、ft2004、d2000、ft2000+、s2500、x100...

DDR--内存种类：DDR3、DDR4、LPDDR4

PBFvxx--飞腾PBF打包工具版本

UEFI/ubootvxxx--飞腾开源UEFI/uboot版本

CPLD/EC/SE--电源管理模块

debug/release--debug版本or release版本

上传文件列表：

d2000-DDR4-UDIMM-PBFv1.63-UEFIv3.5.0-SE-release-D2000+X100白牌机.bin

e2000d-lpddr4-PBFv1.04-uboot1.31-CPLD-debug-4banks-1.bin

e2000d-lpddr4-PBFv1.04-uboot1.31-CPLD-debug-4banks-1.txt

e2000d-lpddr4-PBFv1.04-uboot1.31-CPLD-debug-4banks-2.bin

e2000d-lpddr4-PBFv1.04-uboot1.31-CPLD-debug-4banks-2.txt

e2000d-lpddr4-PBFv1.04-uboot1.31-CPLD-debug-8banks-1.bin

e2000d-lpddr4-PBFv1.04-uboot1.31-CPLD-debug-8banks-1.txt

e2000d-lpddr4-PBFv1.04-uboot1.31-CPLD-debug-8banks-2.bin

e2000d-lpddr4-PBFv1.04-uboot1.31-CPLD-debug-8banks-2.txt

e2000d-lpddr4-PBFv1.04-uboot1.31-EC-debug-SPDdisable.bin

e2000d-lpddr4-PBFv1.04-uboot1.31-EC-debug-SPDdisable.txt

e2000d-lpddr4-PBFv1.04-uboot1.31-EC-debug-SPDenable.bin

e2000d-lpddr4-PBFv1.04-uboot1.31-EC-debug-SPDenable.txt

ft2004-DDR4-UDIMM-PBFv1.84-ubootv1.7.7-CPLD-release-SPDenable-PHYTIUM开发板.bin

ft2004-DDR4-UDIMM-PBFv1.84-ubootv1.7.7-CPLD-release-SPDenable-PHYTIUM开发板.txt

ft2004-DDR4-UDIMM-PBFv2.08-UEFIv3.5.0-CPLD-release-SPDenable-QSPIspeed37.5M-PHYTIUM开发板.bin

ft2004-DDR4-UDIMM-PBFv2.08-UEFIv3.5.0-CPLD-release-SPDenable-QSPIspeed37.5M-PHYTIUM开发板.txt

ft2004-DDR4-UDIMM-PBFv2.08-UEFIv3.5.0-CPLD-release-SPDenable-QSPIspeed4.6M-PHYTIUM开发板.bin

ft2004-DDR4-UDIMM-PBFv2.08-UEFIv3.5.0-CPLD-release-SPDenable-QSPIspeed4.6M-PHYTIUM开发板.txt

x100-LPDDR4-PBFv1.5-PHTIUM-X100显卡.bin

x100-LPDDR4-PBFv1.5-PHTIUM-X100显卡.txt



 **2. 飞腾平台开机启动log** 

  **2.1 飞腾平台开机启动log说明** 

飞腾平台开机启动log是开机过程中，通过调试串口（默认uart1,波特率115200）搜集整理的log文件，其命名规范与飞腾平台固件SFW类似，详细如下：

LOG文件文件名：BootLog-ChipID-DDR-PBFvxx-UEFI/ubootvxxx-CPLD/EC/SE-debug/release.txt 

说明：

BootLog--启动日志

ChipID--飞腾芯片型号如e2000q、e2000d、e2000s、ft2004、d2000、ft2000+、s2500、x100...

DDR--内存种类：DDR3、DDR4、LPDDR4

PBFvxx--飞腾PBF打包工具版本

UEFI/ubootvxxx--飞腾开源UEFI/uboot版本

CPLD/EC/SE--电源管理模块

debug/release--debug版本or release版本



上传文件列表：

BootLog-d2000-DDR4-SODIMM-PBFv1.66-UEFIvxxx-CPLD-release.txt

BootLog-d2000-DDR4-UDIMM-PBFv1.63-UEFIv3.5.0-SE-release-Phytium-D2000+X100白牌机.txt

BootLog-d2000-DDR4-UDIMM-PBFv1.67-ubootv1.36-CPLD-release.txt

BootLog-d2000-DDR4-UDIMM-PBFv1.71-ubootv1.37-CPLD-release.txt

BootLog-e2000d-LPDDR4-RDIMM-CXDB5CCAM-MK_PBFv0.6-uboot1.31-CPLD-release.txt

BootLog-e2000d-LPDDR4-RDIMM-CXDB5CCAM-MK_PBFv1.04-uboot1.31-CPLD-release.txt

BootLog-e2000q-DDR4-SODIMM-PBFv1.03-ubootv1.40-CPLD-release-PHYTIUM-miniITX E2000q开发板.txt

BootLog-e2000q-DDR4-UDIMM-SCB12Q4G160AF-07QI-PBFv1.03-ubootvxxx-CPLD-release-PHYTIUM-COME开发板.txt

BootLog-e2000q-LPDDR4-RDIMM-CXDB5CCAM-MK-PBFv1.00-ubootvxxx-CPLD-release.txt

BootLog-ft2004-DDR3-UDIMM-PBFv1.83-ubootv1.7.5-CPLD-release.txt

BootLog-ft2004-DDR4-UDIMM-PBFv1.71-UEFIv3.5.0-CPLD-release.txt

BootLog-ft2004-DDR4-UDIMM-PBFv1.83-ubootv1.7.7-CPLD-release.txt

BootLog-ft2004-DDR4-UDIMM-PBFv1.84-ubootv1.7.7-CPLD-release.txt

BootLog-ft2004-DDR4-UDIMM-PBFv2.06-1-UEFIv3.5.0-CPLD-debug.txt

BootLog-ft2004-DDR4-UDIMM-PBFv2.06-ubootv2v0.3-CPLD-release.txt

BootLog-ft2004-DDR4-UDIMM-PBFv2.08-ubootv2v0.3-CPLD-release.txt

BootLog-ft2004-DDR4-UDIMM-PBFv2.08-UEFIv3.5.0-CPLD-release.txt

BootLog-X100-DDR4-UDIMM-PBFv1.0-带电源管理.txt

BootLog-X100-DDR4-UDIMM-PBFv1.2-不带电源管理.txt

BootLog-X100-LPDDR4-PBFv1.5.2-不带电源管理.txt

BootLog-X100-LPDDR4-PBFv1.5.2-带电源管理.txt


