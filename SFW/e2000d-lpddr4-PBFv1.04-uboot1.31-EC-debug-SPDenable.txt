/* Author:  lixinde (lxd), lixinde@phytium.com.cn */
PBF : -
BL33 : 
pack date : Mon Jan 30 09:40:54 CST 2023



/****** Pll info ----- Magic : 0x54460020 ----- Version : v0.2 ******/

cpu frq : 2000 MHZ
clust2 frq : 1500 MHZ
lmu frq : 600 MHZ

/*****************************************************************/


/****** Peu info ----- Magic : 0x54460021 ----- Version : v0.4 ******/

independent_tree mode : 0x0
PSU : init-X1X1--RC--RC--Gen3-Gen3--p0-p1
PSU : equalization <0x7-0x7-0x0-0x0>
PEU : init-X1X1X1X1--RC--RC--RC--RC--Gen3-Gen3-Gen3-Gen3--p0-p1-p2-p3
PEU : equalization <0x7-0x7-0x7-0x7>

/*****************************************************************/


/****** Common info ----- Magic : 0x54460013 ----- Version : v0.1 ******/

cluster 2 : core_0  core_1  

/*****************************************************************/


/****** Mcu info ----- Magic : 0x54460024 ----- Version : v0.2 ******/

CH enable             :    CH_0
ECC enable            :    disable
DM enable             :    enable
high performance mode :    enable
2T preamble           :    enable
1 CH driven 2 slot    :    disable
skip scrub            :    disable
PDA invert            :    disable
dq oe timing          :    0x42
mult-rank high mode   :    disable
fix 2x refresh mode   :    disable
auto low power enable :    disable
mem dp reduction en   :    enable
DBI enable            :    disable
train debug           :    phy reg debug
train debug           :    error message
training              :    adaptive training to find the best param
spd force             :    enable
	LPDDR4
	X16/NO ECC/1 Rank/16 Row/10 Column/8 Banks
	dimm_capacity: 2GB
LPDDR4 data swapping:
	data_byte_swap:    0x76541032
	slice0_dq_swizzle: 0x67403251
	slice1_dq_swizzle: 0x23601547
	slice2_dq_swizzle: 0x31072645
	slice3_dq_swizzle: 0x56147230
	slice4_dq_swizzle: 0x67451032
	slice5_dq_swizzle: 0x67403251
	slice6_dq_swizzle: 0x31072645
	slice7_dq_swizzle: 0x56147230

MEM encrypt           :    bypass
TZC                   :    bypass

/*****************************************************************/


/****** Board info ----- Magic : 0x54460015 ----- Version : v0.1 ******/

read spd freq    : enable
 --- <(lmu freq = min(cfg_lmu_pll, spd_freq, max_lmu_freq)>
power ctr source :  EC 
qspi flash speed : 1 - 18.5MHZ 
psu phy :  psu_pcie_c0 + psu_pcie_c1 
gsd phy : 
 gsd_sgmii_0 	MAC_MODE: SGMII  	SPEED: 1G  
 gsd_sgmii_1 	MAC_MODE: SGMII  	SPEED: 1G  
 gsd_dp_0 
 gsd_dp_1 

mio_sel: 0x0.
param_auto_sel: 0x0.


/*****************************************************************/


/****** Pbf info ----- Magic : 0x54460000 ----- Version : v0.1 ******/

primary core set : 0x0
uart_level:  full
log_level :  full
uart baud 115200
/*****************************************************************/
_1clu-2c_PSU-X1X1-PEU-X1X1X1X1
